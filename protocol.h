#pragma once


// Synposis
//    Laser emulator
//
//

#include "laser.h"
#include <sstream>
#include <iostream>
#include <map>
#include <vector>

class Protocol {
private:
    const std::map<Command, std::string> encoding = {{Command::STR,    "STR"},
                                                     {Command::STP,    "STP"},
                                                     {Command::GET_ST, "ST?"},
                                                     {Command::PWR,    "PW="},
                                                     {Command::KAL,    "KAL"},
                                                     {Command::GET_PW, "PW?"},
                                                     {Command::ESM,    "ESM"},
                                                     {Command::DSM,    "DSM"}};

public:
    Query decode(const std::string& line, bool reverted);
    std::string encode(const Response& r);

private:
    std::vector<int> decodeParameters(std::istream& buffer);
    Command          decodeCommand(std::istream& buffer);
    bool             decodeError(const Query& query);

    void encodeCommand(const Response& r, std::ostream& target);
    void encodeParameters(const Response& r, std::ostream& target);
    void encodeError(const Response& r, std::ostream& target);
};




