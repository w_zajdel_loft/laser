#pragma once


#include <vector>
#include <chrono>
#include <thread>
#include <mutex>

enum class Command {
    STR, STP, GET_ST, KAL, PWR, GET_PW, ESM, DSM, OTHER
};

// these are for now identical, maybe later will differ
struct Query {
    std::vector<int> parameters;
    Command          command;
    bool             error;
};

struct Response {
   std::vector<int> parameters;
   Command          command;
   bool             error;
};


// Synposis
//      class encoding logic of laser simulator
//
//


class Laser {

private:
    bool _emits = false;
    bool _silly = false;
    int  _power = 1;

    // keep-alive handling
    std::chrono::high_resolution_clock::time_point _kalUntil;
    std::mutex                                     _kalMutex;


public:
    bool silly() const;
    Response execute(const Query& query);

private:
    void armAutoOffTimer();

    // all of these state-access/change calls return
    //      true: on success
    //      false: on error

    bool readPower(Response& target);
    bool changePower(Response& target, const Query& query);
    bool readEmitState(Response& target);
    bool enableEmitState(Response& target);
    bool disableEmitState(Response& target);
    bool extendEmitState(Response& target);
    bool enableSilly(Response& target);
    bool disableSilly(Response& target);
};



