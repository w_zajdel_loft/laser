
#include "protocol.h"

std::vector<int> Protocol::decodeParameters(std::istream& buffer) {

    auto&& result = std::vector<int>{};
    int item      = 0;

    while ((buffer.get() == '|') && buffer.good()) {
        buffer >> item;
        result.push_back(item);
    }

    return result;
}

Command Protocol::decodeCommand(std::istream& buffer) {

    std::string token;

    // first 3 characters
    token.reserve(3);
    token.push_back(buffer.get());
    token.push_back(buffer.get());
    token.push_back(buffer.get());

    //match aginst known command encodings
    for (auto& item : encoding)
        if (item.second == token)
            return item.first;

    return Command::OTHER;
}


void Protocol::encodeCommand(const Response& r, std::ostream& target) {

    auto it = encoding.find(r.command);
    bool ok = it != encoding.end();

    target << (ok? it->second : "UK");
}


void Protocol::encodeParameters(const Response& r, std::ostream& target) {

    for (auto& v: r.parameters)
        target << "|" << v;
}


void Protocol::encodeError(const Response& r, std::ostream& target) {
    target << (r.error? "!" : "#");
}


// command validation, checks if parameters match command
bool Protocol::decodeError(const Query& query) {
    switch (query.command) {
        case Command::OTHER:    return true;
        case Command::PWR:      return (query.parameters.size() != 1);
        default:                return (query.parameters.size() != 0);
    }
}


Query Protocol::decode(const std::string& line, bool reverted) {

    auto&& result = Query{};
    auto&& lineIn = reverted? std::string{line.rbegin(), line.rend()} : line;
    auto&& buffer = std::istringstream( lineIn );

    result.command    = decodeCommand(buffer);
    result.parameters = decodeParameters(buffer);
    result.error      = decodeError(result);

    return result;
}


std::string Protocol::encode(const Response& r) {

    auto&& buffer = std::ostringstream{};

    encodeCommand(r,buffer);
    encodeParameters(r,buffer);
    encodeError(r,buffer);

    return buffer.str();
}

