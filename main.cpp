#include "laser.h"
#include "protocol.h"

#include <iostream>


int main() {

    std::string lineOut;
    std::string lineIn;


    auto&& device = Laser{};
    auto&& io     = Protocol{};

    while (std::getline(std::cin, lineIn)) {

        bool reverted   = device.silly();
        auto&& query    = io.decode(lineIn, reverted);
        auto&& response = device.execute(query);
        auto&& lineOut  = io.encode(response);

        std::cout << lineOut << std::endl;
    }

    return 0;
}
