#include "laser.h"

// this is the most interesting one
Response Laser::execute(const Query& query) {

    auto&& result  = Response{};
    result.command = query.command;
    result.error   = query.error;

    if (query.error) {
        result.parameters = query.parameters;
        return result;
    }

    switch (query.command) {

    case Command::OTHER:  result.error = true;                        break;
    case Command::ESM:    result.error = !enableSilly(result);        break;
    case Command::DSM:    result.error = !disableSilly(result);       break;
    case Command::PWR:    result.error = !changePower(result, query); break;
    case Command::GET_PW: result.error = !readPower(result);          break;
    case Command::GET_ST: result.error = !readEmitState(result);      break;
    case Command::STR:    result.error = !enableEmitState(result);    break;
    case Command::STP:    result.error = !disableEmitState(result);   break;
    case Command::KAL:    result.error = !extendEmitState(result);    break;

    }

    if (_emits) {
        armAutoOffTimer();
    }

    return result;
}



void Laser::armAutoOffTimer () {

    auto timer = std::thread([this]{

        // here we capture this pointer --
        //      to make this proper, destructor of the laser class
        //      should first "join" all timer-threads
        //      to prevent thread-lambda running with dangling this pointer
        //
        //      I skip this handling, since the emulator exectunble will be killed by SIGTERM anyway
        //

        std::this_thread::sleep_for(std::chrono::seconds(5));
        std::lock_guard<std::mutex> lock(_kalMutex);

        if (_kalUntil < std::chrono::high_resolution_clock::now())
            _emits = false;
        });

    timer.detach();
}


bool Laser::readPower(Response& target) {
    std::lock_guard<std::mutex> lock(_kalMutex);

    target.parameters.push_back( _emits? _power: 0);
    return true;
}

bool Laser::changePower(Response& target, const Query& query) {

    int toLevel  = query.parameters[0];
    bool inRange = (toLevel > 0  && toLevel < 100);

    _power = inRange? toLevel : _power;
    return inRange;
}


bool Laser::readEmitState(Response& target) {
    std::lock_guard<std::mutex> lock(_kalMutex);

    target.parameters.push_back( _emits? 1: 0);
    return true;
}

bool Laser::enableEmitState(Response& target) {
    std::lock_guard<std::mutex> lock(_kalMutex);

    _emits    = true;
    _kalUntil = std::chrono::high_resolution_clock::now() + std::chrono::seconds(5);

    return true;  // alternative: fail if already running
}

bool Laser::disableEmitState(Response& target) {
    std::lock_guard<std::mutex> lock(_kalMutex);

    _emits = false;

    return true; // alternative: faile is already stopped
}

bool Laser::extendEmitState(Response& target) {
    std::lock_guard<std::mutex> lock(_kalMutex);

    if (_emits) {
        _kalUntil    = std::chrono::high_resolution_clock::now() + std::chrono::seconds(5);
    }

    return _emits;  // alternative: do not fail if not emits
}

bool Laser::enableSilly(Response& target) {
    _silly = true;
    return true;
}

bool Laser::disableSilly(Response& target) {
    _silly = false;
    return true;
}

bool Laser::silly() const {
    return _silly;
}


